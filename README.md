# Parking Lot Problem

First, check if you have node and npm installed by using this commands :
node -v
npm -v

clone repo and then open terminal and type the following:

1. cd parking_lot : Navigates to the `parking_lot` root folder.
2. npm install : Installs all the dependencies.
3. npm run start : Starts the console application.
4. status
5. create_parking_lot 6
6. park KA-01-HH-1234 White
7. park KA-01-HH-9999 White
8. park KA-01-BB-0001 Black
9. park KA-01-HH-7777 Red
10.park KA-01-HH-2701 Blue
11. park KA-01-HH-3141 Black
12. leave 4
13. status
14. park KA-01-P-333 White
15. park DL-12-AA-9999 White
16. registration_numbers_for_cars_with_colour White
17. slot_numbers_for_cars_with_colour White
18. slot_number_for_registration_number KA-01-HH-3141
19. slot_number_for_registration_number MH-04-AY-1111

This is inputs given by me to test problem. however you can give your inputs and check if testcases works or not.

Thank you
