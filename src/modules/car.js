
class Car {
    constructor (NUMBER, COLOR) {
        this.NUMBER = NUMBER; // unique 
        this.COLOR = COLOR;
    }

 
    isCarEqual (carA, carB) {
        return ((carA.NUMBER.toLowerCase() === carB.NUMBER.toLowerCase())
            && carA.COLOR.toLowerCase() === carB.toLowerCase());
    }
}

module.exports = Car;
